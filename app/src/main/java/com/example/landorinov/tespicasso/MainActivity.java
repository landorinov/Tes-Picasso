package com.example.landorinov.tespicasso;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final String nama_tour_guide[] = {
            "Yudi Herdiansyah",
            "Asep Budi",
            "Panji Maulana",
            "Angga Perkasa",
            "Geta Margadikara",
            "Egi Herlianto Putra",
            "Asep Reza",
            "Mulyana Supriyanto",
            "Ade Maulana",
            "Robby Rodhiyan"

    };

    private final String jenis_kelamin_tour_guide[] = {
            "Pria",
            "Pria",
            "Pria",
            "Pria",
            "Pria",
            "Pria",
            "Pria",
            "Pria",
            "Pria",
            "Pria"
    };

    private final String rating_tour_guide[] = {
            "Good",
            "Excelent",
            "Excelent",
            "Bad",
            "Good",
            "Bad",
            "Bad",
            "Bad",
            "Good",
            "Excelent"
    };

    private final String img_tour_guide[] = {
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2RZRW7ET_aWFSC6Za6Nk_QtaiSlVHYU6JSUf1PJMYA3SvKFTOpQ"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

    }

    private void initViews() {
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        ArrayList tourguides = prepareData();
        DataAdapter adapter = new DataAdapter(getApplicationContext(),tourguides);
        recyclerView.setAdapter(adapter);
    }

    private ArrayList prepareData(){

        ArrayList tour_guide = new ArrayList<>();
        for(int i=0;i<nama_tour_guide.length;i++){
            TourGuide tourGuide = new TourGuide();
            tourGuide.setNama_tour_guide(nama_tour_guide[i]);
            tourGuide.setJenis_kelamin_tour_guide(jenis_kelamin_tour_guide[i]);
            tourGuide.setRating_tour_guide(rating_tour_guide[i]);
            tourGuide.setTour_guide_img_url(img_tour_guide[i]);
            tour_guide.add(tourGuide);
        }
        return tour_guide;
    }

}
