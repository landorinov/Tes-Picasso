package com.example.landorinov.tespicasso;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Lando Rinov on 10/9/2017.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private ArrayList<TourGuide> tour_guides;
    private Context context;

    public DataAdapter (Context context, ArrayList<TourGuide> tour_guides) {
        this.context = context;
        this.tour_guides = tour_guides;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        ImageButton detailTourGuide = (ImageButton) view.findViewById(R.id.detail_tour_guide);
        detailTourGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context.getApplicationContext(),"Detail Button",Toast.LENGTH_LONG).show();
            }
        });
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        viewHolder.nama_tour_guide.setText(tour_guides.get(i).getNama_tour_guide());
        viewHolder.jenis_kelamin_tour_guide.setText(tour_guides.get(i).getJenis_kelamin_tour_guide());
        viewHolder.rating_tour_guide.setText(tour_guides.get(i).getRating_tour_guide());
        Picasso.with(context).load(tour_guides.get(i).getTour_guide_img_url()).resize(100, 100).into(viewHolder.img_tour_guide);
    }

    @Override
    public int getItemCount() {
        return tour_guides.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView nama_tour_guide;
        TextView jenis_kelamin_tour_guide;
        TextView rating_tour_guide;
        ImageView img_tour_guide;
        public ViewHolder(View view) {
            super(view);

            nama_tour_guide = (TextView)view.findViewById(R.id.nama_tour_guide);
            jenis_kelamin_tour_guide = (TextView)view.findViewById(R.id.jenis_kelamin_tour_guide);
            rating_tour_guide = (TextView)view.findViewById(R.id.rating_tour_guide);
            img_tour_guide= (ImageView)view.findViewById(R.id.img_tour_guide);
        }
    }


}
