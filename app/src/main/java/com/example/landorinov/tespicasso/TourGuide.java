package com.example.landorinov.tespicasso;

/**
 * Created by Lando Rinov on 10/9/2017.
 */

public class TourGuide {

    private String nama_tour_guide;
    private String jenis_kelamin_tour_guide;
    private String rating_tour_guide;
    private String tour_guide_img_url;

    public String getNama_tour_guide() {
        return nama_tour_guide;
    }

    public void setNama_tour_guide(String nama_tour_guide) {
        this.nama_tour_guide = nama_tour_guide;
    }

    public String getJenis_kelamin_tour_guide() {
        return jenis_kelamin_tour_guide;
    }

    public void setJenis_kelamin_tour_guide(String jenis_kelamin_tour_guide) {
        this.jenis_kelamin_tour_guide = jenis_kelamin_tour_guide;
    }

    public String getRating_tour_guide() {
        return rating_tour_guide;
    }

    public void setRating_tour_guide(String rating_tour_guide) {
        this.rating_tour_guide = rating_tour_guide;
    }

    public String getTour_guide_img_url() {
        return tour_guide_img_url;
    }

    public void setTour_guide_img_url(String tour_guide_img_url) {
        this.tour_guide_img_url = tour_guide_img_url;
    }
}
